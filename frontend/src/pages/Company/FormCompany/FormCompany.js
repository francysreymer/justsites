import React, { useState, useEffect } from 'react'; 
import './FormCompany.scss';
import axios from 'axios';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CheckIcon from '@material-ui/icons/Check';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import API from '../../../config/api';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DeleteItem from '../../../components/DeleteItem/DeleteItem';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    width: 800,
    borderRadius: '4px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const FormCompany = (props) => {

  const classes = useStyles();

  const initialState = {
    name: '',
    document: '',
    responsible: '',
    responsible_email: '',
    responsible_phone: '',
    zip_code: '',
    city: '',
    state: '',
    address: '',
    number: '',
    complement: '',
    neighborhood: '',
    observation: ''
  };

  const [
    {
      name,
      document,
      responsible,
      responsible_email,
      responsible_phone,
      zip_code,
      city,
      state,
      address,
      number,
      complement,
      neighborhood,
      observation
    },
    setState
  ] = useState(initialState);

  const clearState = () => {
    setState({ ...initialState });
  };

  const onChange = e => {
    const { name, value } = e.target;
    setState(prevState => ({ ...prevState, [name]: value }));

    if(name === 'zip_code') {
      if(value.length === 8) {
        getCep(value); 
      } else if(!value || value.length !== 8) {
        clearFormCep()
      }
    }
  };

  const [messageForm, setMessageForm] = useState('');
  const [typeMessageForm, setTypeMessageForm] = useState('');
  const [openDelete, setOpenDelete] = useState(false);
  const [openForm, setOpenForm] = useState(false);

  useEffect(function() {
    if(!props.editId && !props.deleteId && props.modal) {
      clearState()
      setOpenForm(true)
    } else if(props.deleteId && !props.modal) {
      setOpenDelete(false)
    } else if(props.deleteId && props.modal) {
      setOpenDelete(true)
    } else if(!props.deleteId && props.modal) {
      setOpenForm(true)
    } else if(!props.modal) {
      setOpenForm(false)
    }
  }, [props.modal]);

  useEffect(function() {
    async function fetchData() {
      if(props.editId) {
        const user = JSON.parse(localStorage.getItem('user'));
        const response = await axios.get(API.companies.read+props.editId, {
          headers: {
            "x-access-token": user.token
          }
        });
        if(response.data.result[0].id) {
          setState({ ...response.data.result[0] });
        } else {
          setMessageForm('Erro ao carregar dados!')
          setTypeMessageForm('error')
          setOpenAlert(true)
          props.modalClose()
        }
      }
    }
    fetchData();
  }, [props.editId]);

  useEffect(function() {
    if(props.deleteId && props.modal) {
      setOpenForm(false)
      setOpenDelete(true)
    }
  }, [props.deleteId]);

  const handleSave = async () => {
    const form = {
      name: name,
      document: document,
      responsible: responsible,
      responsible_email: responsible_email,
      responsible_phone: responsible_phone,
      zip_code: zip_code,
      city: city,
      state: state,
      address: address,
      number: number,
      complement: complement,
      neighborhood: neighborhood,
      observation: observation
    }

    console.log('save form: ', form)

    let response = [];
    const user = JSON.parse(localStorage.getItem('user'));
    if(props.editId) {
      response = await axios.put(API.companies.edit+props.editId, form, {
        headers: {
          "x-access-token": user.token
        }
      });
      console.log('save edit response: ', response)
    } else {
      response = await axios.post(API.companies.add, form, {
        headers: {
          "x-access-token": user.token
        }
      });
      console.log('save edit response: ', response)
    }

    if((!props.editId && response.data.result.id) || (props.editId && response.data.result[0] === 1)) {
      setMessageForm('Os dados foram salvos.')
      setTypeMessageForm('success')
      setOpenAlert(true)
      props.modalClose()
      props.editId ? props.updateTable(props.editId, form, 'edit') : props.updateTable(response.data.result.id, form, 'add')
    } else {
      setMessageForm('Os dados não foram salvos!')
      setTypeMessageForm('error')
      setOpenAlert(true)
    }
  };

  function clearFormCep() {
    setState(prevState => ({ ...prevState, 'city': '' }));
    setState(prevState => ({ ...prevState, 'state': '' }));
    setState(prevState => ({ ...prevState, 'address': '' }));
    setState(prevState => ({ ...prevState, 'neighborhood': '' }));
  }

  async function getCep(code) {
    setState(prevState => ({ ...prevState, 'city': '...' }));
    setState(prevState => ({ ...prevState, 'state': '...' }));
    setState(prevState => ({ ...prevState, 'address': '...' }));
    setState(prevState => ({ ...prevState, 'neighborhood': '...' }));

    const result = await axios.get(`https://viacep.com.br/ws/${code}/json/`);

    if(result.data.erro) {
      clearFormCep()
      alert('CEP não encontrado!')  
    } else {
      setState(prevState => ({ ...prevState, 'city': result.data.localidade }));
      setState(prevState => ({ ...prevState, 'state': result.data.uf }));
      setState(prevState => ({ ...prevState, 'address': result.data.logradouro }));
      setState(prevState => ({ ...prevState, 'neighborhood': result.data.bairro }));
    }
  }  

  const [openAlert, setOpenAlert] = useState(false);
  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenAlert(false);
  };

  const handleDelete = async () => {
    const user = JSON.parse(localStorage.getItem('user'));
    const response = await axios.delete(API.companies.delete+props.deleteId, {
      headers: {
        "x-access-token": user.token
      }
    });
    console.log('delete response: ', response)
    
    if(response.data.result[0] === 1) {
      setMessageForm('O dado foi excluído.')
      setTypeMessageForm('success')
      setOpenAlert(true)
      props.modalClose()
      props.updateTable(props.deleteId, null, 'delete')
    } else {
      setMessageForm('O dado não foi excluído!')
      setTypeMessageForm('error')
      setOpenAlert(true)
    }
  }

  return (
    <div>
      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
        <MuiAlert elevation={6} variant="filled" onClose={handleCloseAlert} severity={typeMessageForm} >
          {messageForm}
        </MuiAlert>          
      </Snackbar>
      <DeleteItem 
          modal={openDelete}
          modalClose={props.modalClose}
          handleDelete={handleDelete}
          handleOpenDelete={props.handleOpenDelete}
      />
      <div className="">
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={openForm}
          onClose={() => props.modalClose()}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openForm}>
            <div className={classes.paper}>
              <div className="form-title-group">
                <div className="form-title">
                  Cadastro | Empresa
                </div>
                <div className="form-title-close">
                  <CloseIcon 
                    onClick={() => props.modalClose()} 
                    className="button-close" 
                  />
                </div>
              </div>
              <div className="form-content">
                <div className="content-form">
                  <form>
                    <div className="form-row">
                      <div className="form-group col-md-8">
                        <label htmlFor="name" className="col-form-label col-form-label-sm">Nome da Empresa</label>
                        <input type="text" className="form-control form-control-sm" placeholder="Nome da Empresa" id="name" name="name" value={name} onChange={onChange} />
                      </div>
                      <div className="form-group col-md-4">
                        <label htmlFor="document" className="col-form-label col-form-label-sm">CNPJ</label>
                        <input type="text" className="form-control form-control-sm" placeholder="CNPJ" id="document" name="document" value={document} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-5">
                        <label htmlFor="responsible" className="col-form-label col-form-label-sm">Nome do Responsável</label>
                        <input type="text" className="form-control form-control-sm" id="responsible" name="responsible" value={responsible} onChange={onChange} />
                      </div>
                      <div className="form-group col-md-4">
                        <label htmlFor="responsible_email" className="col-form-label col-form-label-sm">Email</label>
                        <input type="text" className="form-control form-control-sm" id="responsible_email" name="responsible_email" value={responsible_email} onChange={onChange} />
                      </div>
                      <div className="form-group col-md-3">
                        <label htmlFor="responsible_phone" className="col-form-label col-form-label-sm">Fone</label>
                        <input type="text" className="form-control form-control-sm" placeholder="(00) 00000-0000" id="responsible_phone" name="responsible_phone" value={responsible_phone} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-2">
                        <label htmlFor="zip_code" className="col-form-label col-form-label-sm">CEP</label>
                        <input type="text" className="form-control form-control-sm" placeholder="00000-000" id="zip_code" name="zip_code" value={zip_code} onChange={onChange} />
                      </div>
                      <div className="form-group col-md-6">
                        <label htmlFor="city" className="col-form-label col-form-label-sm">Cidade</label>
                        <input type="text" className="form-control form-control-sm" id="city" name="city" value={city} onChange={onChange} />
                      </div>
                      <div className="form-group col-md-4">
                        <label htmlFor="state" className="col-form-label col-form-label-sm">Estado</label>
                        <input type="text" className="form-control form-control-sm" id="state" name="state" value={state} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <label htmlFor="address" className="col-form-label col-form-label-sm">Endereço</label>
                        <input type="text" className="form-control form-control-sm" id="address" name="address" value={address} onChange={onChange} />
                      </div>
                      <div className="form-group col-md-2">
                        <label htmlFor="number" className="col-form-label col-form-label-sm">Número</label>
                        <input type="text" className="form-control form-control-sm" id="number" name="number" value={number} onChange={onChange} />
                      </div>
                      <div className="form-group col-md-4">
                        <label htmlFor="complement" className="col-form-label col-form-label-sm">Complemento</label>
                        <input type="text" className="form-control form-control-sm" id="complement" name="complement" value={complement} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="neighborhood" className="col-form-label col-form-label-sm">Bairro</label>
                        <input type="text" className="form-control form-control-sm" id="neighborhood" name="neighborhood" value={neighborhood} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="observation" className="col-form-label col-form-label-sm">Observação</label>
                        <textarea className="form-control" id="observation" name="observation" rows="3" value={observation} onChange={onChange}></textarea>
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div className="form-button-group">
                <div className="form-button-delete">
                  {(!props.editId) ? '' :
                  <Button
                    variant="contained"
                    color="secondary"
                    size="small"
                    startIcon={<DeleteIcon />}
                    onClick={() => props.handleOpenDelete(props.editId)}
                    component="div"
                  >
                    Excluir
                  </Button>
                  }
                </div>
                <div className="form-buttons">
                  <Button
                    variant="contained"
                    color="default"
                    size="small"
                    startIcon={<CancelIcon />}
                    onClick={() => props.modalClose()}
                    component="div"
                  >
                    Cancelar
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    className="buttonSave"
                    startIcon={<CheckIcon />}
                    onClick={handleSave}
                    component="div"
                  >
                    Salvar
                  </Button>
                </div>
              </div>
            </div>
          </Fade>
        </Modal>
      </div>
    </div>
  );
}

export default FormCompany;