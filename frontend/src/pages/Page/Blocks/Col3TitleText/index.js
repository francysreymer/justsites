import React, { useState, useEffect } from 'react'; 
import Input from '@material-ui/core/Input';
import AWS from 'aws-sdk';
import EditorText from '../../../../components/TinyMCE';
import '../index.scss';

var albumBucketName = "multisites";
var bucketRegion = "sa-east-1";
var IdentityPoolId = "sa-east-1:45a0b1c1-109e-4348-93ec-1c5975ae37eb";

AWS.config.update({
  region: bucketRegion,
  credentials: new AWS.CognitoIdentityCredentials({
    IdentityPoolId: IdentityPoolId
  })
});

const Col3TitleText = (props) => {

  let initialState = {
    title_1: '',
    title_2: '',
    title_3: '',
    text_1: '',
    text_2: '',
    text_3: '',
  };

  if(props.content.fields) {
    initialState = {
      title_1: props.content.fields.title_1,
      title_2: props.content.fields.title_2,
      title_3: props.content.fields.title_3,
      text_1: props.content.fields.text_1,
      text_2: props.content.fields.text_2,
      text_3: props.content.fields.text_3,
    };
  }

  const [
    {
        title_1,
        title_2,
        title_3,
        text_1,
        text_2,
        text_3,
    },
    setState
  ] = useState(initialState);

  const clearState = () => {
    setState({ ...initialState });
  };

  useEffect(function() {
    clearState()
  }, [props.content]);

  const buildContent = () => {

    const content = {
        fields: {
            title_1: title_1,
            title_2: title_2,
            title_3: title_3,
            text_1: text_1,
            text_2: text_2,
            text_3: text_3,
        },
        html: '<div class="Col3TitleText">'+

          '<div class="form-row">'+
          '<div class="form-group col-md-4">'+
          '<div class="image_1">'+title_1+'</div>'+
          '</div>'+
          '<div class="form-group col-md-4">'+
          '<div class="image_2">'+title_2+'</div>'+
          '</div>'+
          '<div class="form-group col-md-4">'+
          '<div class="image_3">'+title_3+'</div>'+
          '</div>'+
          '</div>'+


            '<div class="form-row">'+
            '<div class="form-group col-md-4">'+
            '<div class="title_1">'+title_1+'</div>'+
            '</div>'+
            '<div class="form-group col-md-4">'+
            '<div class="title_2">'+title_2+'</div>'+
            '</div>'+
            '<div class="form-group col-md-4">'+
            '<div class="title_3">'+title_3+'</div>'+
            '</div>'+
            '</div>'+
            '<div class="form-row">'+
            '<div class="form-group col-md-4">'+
            '<div class="text_1">'+text_1+'</div>'+
            '</div>'+
            '<div class="form-group col-md-4">'+
            '<div class="text_2">'+text_2+'</div>'+
            '</div>'+
            '<div class="form-group col-md-4">'+
            '<div class="text_3">'+text_3+'</div>'+
            '</div>'+
            '</div>'+
            '</div>',
    }
    props.changeBlock(content)
  };

  useEffect(() => { buildContent() }, [
    title_1,
    title_2,
    title_3,
    text_1,
    text_2,
    text_3,])

  const onChange = (content, name) => {
    setState(prevState => ({ ...prevState, [name]: content }));
  };

  const changeUpload = async e => {
    const { files } = e.target;
    console.log("changeUpload e: ", e);

    //var files = document.getElementById("photoupload").files;
    if (!files.length) {
      return alert("Please choose a file to upload first.");
    }
    var file = files[0];
    var fileName = file.name;
    var albumPhotosKey = encodeURIComponent('images') + "/";
  
    var photoKey = albumPhotosKey + fileName;
  
    // Use S3 ManagedUpload class as it supports multipart uploads
    var upload = new AWS.S3.ManagedUpload({
      params: {
        Bucket: albumBucketName,
        Key: photoKey,
        Body: file
      }
    });

    try {
  
      let data = await upload.promise();
      console.log("data: ", data)

    } catch(error) {
      console.log("error: ", error)
    }
  
    
  }

  return (
    <form className="form">
        <div className="form-row">
            <div className="form-group col-md-4">
                <div className="title-block">Imagem 1:</div>
                <Input id="image-1" type="file" onChange={changeUpload} />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Imagem 2:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'title_2')} initialValue={title_2} type="title" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Imagem 3:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'title_3')} initialValue={title_3} type="title" />
            </div>
        </div>
        <div className="form-row">
            <div className="form-group col-md-4">
                <div className="title-block">Título 1:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'title_1')} initialValue={title_1} type="title" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Título 2:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'title_2')} initialValue={title_2} type="title" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Título 3:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'title_3')} initialValue={title_3} type="title" />
            </div>
        </div>
        <div className="form-row">
            <div className="form-group col-md-4">
                <div className="title-block">Conteúdo 1:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'text_1')} initialValue={text_1} type="text" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Conteúdo 2:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'text_2')} initialValue={text_2} type="text" />
            </div>
            <div className="form-group col-md-4">
                <div className="title-block">Conteúdo 3:</div>
                <EditorText changeEditorText={(content) => onChange(content, 'text_3')} initialValue={text_3} type="text" />
            </div>
        </div>
    </form>
  );
}

export default Col3TitleText;