const { equal, deepEqual, ok } = require('assert');
const Sequelize = require('sequelize');
const TypeModel = require('../src/Model/Type');
const TypesController = require('../src/Controller/TypesController');
const moment = require('moment');

describe('Types CRUD', function() {
	this.timeout(Infinity);
	before(async () => {
		const connection = await TypesController.connect()
		const model = await TypesController.defineModel(connection, TypeModel)
		context = new TypesController(connection, model);

		await context.create({name:"Jane", created:new Date(), modified:new Date()});
	});

	it('MySQL connection', async () => {
		const result = await context.isConnected();
		equal(result, true);
	});

	/*it('CREATE', async () => {
		const item = {name:"Jane", created:new Date(), modified:new Date()};
		console.log('create item: ', item);
		let result = await context.create(item);
		delete result.dataValues.id;
		console.log('create item 2: ', item);
		console.log('result.dataValues: ', result.dataValues);
		console.log('moment().format(): ', moment().format());
		deepEqual(result.dataValues, item);
	});*/

  it('READ', async () => {
	const item = {id: 24, name: "Jane"};
    const [result] = await context.read(item);

    delete result.dataValues.created;
    delete result.dataValues.modified;

    deepEqual(result.dataValues, item);
  });

  it('UPDATE', async () => {
	const item = {name: "Jane UPDATED"};
	const id = 25;
    const [result] = await context.update(id, item);
    deepEqual(result, 1);
  });

  it('DELETE', async () => {
    const [item] = await context.read({});
    const result = await context.delete(item.id);
    deepEqual(result, 1);
  });

});
