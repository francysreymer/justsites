const BlogModel = async (Sequelize) => {
  const database = require('../../config/database.json');
  const sequelize = await new Sequelize(database.url);

  const Blog = sequelize.define('category', 
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true,
      },
      title: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },  
      content: {
        type: Sequelize.TEXT,
        required: true,
        allowNull: false
      }, 
      categories: {
        type: Sequelize.JSON,
      }, 
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      company_id: {
        type: Sequelize.INTEGER,
        references: {
          // This is a reference to another model
          model: 'companies',
          // This is the column name of the referenced model
          key: 'id',
        }
      },
      updated_at_formatted: {
        type: Sequelize.VIRTUAL,
        get() {
          return this.updated_at.toLocaleString('pt-BR')
        },
        set(value) {
          throw new Error('Do not try to set the `updated_at_formatted` value!');
        }
      }, 
    }, 
    { 
      tableName: 'blogs',
      freezeTableName: false,
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',  
    }
  )

  await Blog.sync()

  return {Blog, sequelize}
}

module.exports = BlogModel