const ThemeModel = async (Sequelize) => {
  const database = require('../../config/database.json');
  const sequelize = await new Sequelize(database.url);

  const Theme = sequelize.define('theme', 
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      css_file: {
        type: Sequelize.TEXT,
        allowNull: true
      },   
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }, 
    },
    { 
      tableName: 'themes',
      freezeTableName: false,
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',  
    }
)

await Theme.sync()

return {Theme, sequelize}
}

module.exports = ThemeModel