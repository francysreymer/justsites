import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
    overrides: {
        MuiTypography: {
            body1: {
                fontSize: '12px',
                color: '#000',
            },
        }
    }
})

export default theme