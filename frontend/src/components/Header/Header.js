import React, { useState } from 'react'; 
import { Link } from "react-router-dom";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from 'reactstrap';
import userImg from '../../assets/img/user.png';
import './Header.scss';

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);
  const user = JSON.parse(localStorage.getItem('user'));
  const nameUser = user ? user.user.name : "";
  const [name, setName] = useState(nameUser);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <>
      <Navbar color="primary" dark light expand="md">
        <NavbarBrand href="/">JustSites</NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="mr-auto" navbar>
          </Nav>
          <Nav navbar>
            <UncontrolledDropdown nav inNavbar>
            <div className="div-user">
              <div className="div-img"><img src={userImg} alt="Usuário" /></div>
              <div className="div-username">
                <DropdownToggle nav caret>
                  {name}     
                </DropdownToggle>
              </div>
            </div>
              <DropdownMenu right>
                <DropdownItem>
                  <Link to="/users">Usuários</Link>  
                </DropdownItem>
                <DropdownItem divider />
                <DropdownItem>
                  <Link to="/login">Sair</Link>
                </DropdownItem>
              </DropdownMenu>
            </UncontrolledDropdown>
          </Nav>
        </Collapse>
      </Navbar>
    </>
  );
}

export default Header;