import React from 'react'; 
import Header from '../../Header/Header'
import Page from '../../../pages/Page'

const LayoutPage = (props) => {

  return (
    <>
      <Header />
      <Page />
    </>
  );
}

export default LayoutPage;