import React, { useState, useEffect } from 'react'; 
import { Redirect } from 'react-router-dom'
import Header from '../Header/Header'
import Sidebar from '../Sidebar/Sidebar'
import Site from '../../pages/Site'
import Company from '../../pages/Company'
import Theme from '../../pages/Theme'
import Blog from '../../pages/Blog'
import Category from '../../pages/Category'
import User from '../../pages/User'
import LayoutPage from './Page';

const Layout = (props) => {

  const [user, setUser] = useState(true);

  useEffect(function() {
    //const userLogin = JSON.parse(localStorage.getItem('user'));
    //const userExist = userLogin ? true : false;
    const userExist = true;
    setUser(userExist)
  }, [props.component]);

  const component = () => {
    switch(props.component) {
      case "Company":         return <><Header /><Sidebar /><div className="content"><Company /></div></>;
      case "Theme":           return <><Header /><Sidebar /><div className="content"><Theme /></div></>;
      case "Site":            return <><Header /><Sidebar /><div className="content"><Site /></div></>;
      case "Blog":            return <><Header /><Sidebar /><div className="content"><Blog /></div></>;
      case "Category":        return <><Header /><Sidebar /><div className="content"><Category /></div></>;
      case "User":            return <><Header /><Sidebar /><div className="content"><User /></div></>;
      case "LayoutPage":      return <LayoutPage />;
      default:                return ''
    }
  }

  return (
    <>
      {user === true ? component() : <Redirect to="/login" />}
    </>
  );
}

export default Layout;