const SiteModel = async (Sequelize) => {
  const database = require('../../config/database.json');
  const sequelize = await new Sequelize(database.url);

  const Site = sequelize.define('site', 
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      domain: {
        type: Sequelize.STRING,
        allowNull: true
      },   
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      },
      theme_id: {
        type: Sequelize.INTEGER,
        references: {
          // This is a reference to another model
          model: 'themes',
          // This is the column name of the referenced model
          key: 'id',
        }
      },
      company_id: {
        type: Sequelize.INTEGER,
        references: {
          // This is a reference to another model
          model: 'companies',
          // This is the column name of the referenced model
          key: 'id',
        }
      } 
    }, 
    { 
      tableName: 'sites',
      freezeTableName: false,
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',  
    }, 
  )

  await Site.sync()

  return {Site, sequelize}
}

module.exports = SiteModel