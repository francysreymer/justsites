import React from 'react';
import { Link } from "react-router-dom";
import { ThemeProvider } from '@material-ui/styles'
import { makeStyles } from '@material-ui/core/styles';
import theme from './theme.js'
import ListSubheader from '@material-ui/core/ListSubheader';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import FormatBoldIcon from '@material-ui/icons/FormatBold';
import LabelIcon from '@material-ui/icons/Label';
import LanguageIcon from '@material-ui/icons/Language';
import ColorLensIcon from '@material-ui/icons/ColorLens';
import BusinessIcon from '@material-ui/icons/Business';
import './Sidebar.scss';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '256px',
    maxWidth: 360,
    fontSize: '8px !important',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function Sidebar() {
  const classes = useStyles();
 
  return (
    <>
      <div className="div-menu">
        <List
          component="nav"
          aria-labelledby="nested-list-subheader"
          subheader={
            <ListSubheader component="div" id="nested-list-subheader">
              Super Admin
            </ListSubheader>
          }
          className={classes.root}
        >
          <ThemeProvider theme={theme}>
            <ListItem button>
              <ListItemIcon>
                <BusinessIcon />
              </ListItemIcon>
              <Link to="/companies"><ListItemText primary="Empresas" /></Link>
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <ColorLensIcon />
              </ListItemIcon>
              <Link to="/themes"><ListItemText primary="Temas" /></Link>
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <LanguageIcon />
              </ListItemIcon>
              <Link to="/sites"><ListItemText primary="Sites" /></Link>
            </ListItem>
          </ThemeProvider>
        </List>
        <List
          component="nav"
          aria-labelledby="nested-list-subheader"
          subheader={
            <ListSubheader component="div" id="nested-list-subheader">
              Conteúdo
            </ListSubheader>
          }
          className={classes.root}
        >
          <ThemeProvider theme={theme}>
            <ListItem button>
              <ListItemIcon>
                <FormatBoldIcon />
              </ListItemIcon>
              <Link to="/blogs"><ListItemText primary="Blogs" /></Link>
            </ListItem>
            <ListItem button>
              <ListItemIcon>
                <LabelIcon />
              </ListItemIcon>
              <Link to="/categories"><ListItemText primary="Categories" /></Link>
            </ListItem>
          </ThemeProvider>
        </List>
      </div>
      </>
  );
}
