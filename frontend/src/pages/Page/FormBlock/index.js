import React, { useState, useEffect } from 'react'; 
import './index.scss';
import axios from 'axios';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CheckIcon from '@material-ui/icons/Check';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import ArrowBackIcon from '@material-ui/icons/ArrowBack';
import API from '../../../config/api';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DeleteItem from '../../../components/DeleteItem/DeleteItem';
import { useParams } from "react-router";
import Col3TitleText from '../Blocks/Col3TitleText/';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    width: 800,
    borderRadius: '4px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const FormPage = (props) => {

  const classes = useStyles();

  const initialState = {
    name: '',
    content: '',
    page_id: props.pageId,
  };

  const [
    {
      name,
      content,
      page_id,
    },
    setState
  ] = useState(initialState);

  const clearState = () => {
    setState({ ...initialState });
  };

  const onChange = e => {
    const { name, value } = e.target;
    setState(prevState => ({ ...prevState, [name]: value }));
  };

  const [messageForm, setMessageForm] = useState('');
  const [typeMessageForm, setTypeMessageForm] = useState('');
  const [openDelete, setOpenDelete] = useState(false);
  const [openForm, setOpenForm] = useState(false);
  const [selectedBlock, setSelectedBlock] = useState(false);

  useEffect(function() {
    if(!props.editId && !props.deleteId && props.modal) {
      clearState()
      setOpenForm(true)
      setSelectedBlock(false)
    } else if(props.deleteId && !props.modal) {
      setOpenDelete(false)
    } else if(props.deleteId && props.modal) {
      setOpenDelete(true)
    } else if(!props.deleteId && props.modal) {
      setOpenForm(true)
    } else if(!props.modal) {
      setOpenForm(false)
    }
  }, [props.modal]);

  useEffect(function() {
    clearState()
    async function fetchData() {
      if(props.editId) {
        setSelectedBlock(true)
        const user = JSON.parse(localStorage.getItem('user'));
        const response = await axios.get(API.blocks.read+props.editId, {
          headers: {
            "x-access-token": user.token
          }
        });
        if(response.data.result[0].id) {
          setState({ ...response.data.result[0] });
        } else {
          setMessageForm('Erro ao carregar dados!')
          setTypeMessageForm('error')
          setOpenAlert(true)
          props.modalClose()
        }
      }
    }
    fetchData();
  }, [props.editId]);

  useEffect(function() {
    if(props.deleteId && props.modal) {
      setOpenForm(false)
      setOpenDelete(true)
    }
  }, [props.deleteId]);

  const handleSave = async () => {
    const form = {
      name: name,
      content: content, 
      page_id: page_id, 
    }

    let response = [];
    const user = JSON.parse(localStorage.getItem('user'));
    if(props.editId) {
      response = await axios.put(API.blocks.edit+props.editId, form, {
        headers: {
          "x-access-token": user.token
        }
      });
      console.log('save edit response: ', response)
    } else {
      console.log('save form else API.blocks.add: ', API.blocks.add)
      response = await axios.post(API.blocks.add, form, {
        headers: {
          "x-access-token": user.token
        }
      });
      console.log('save add response: ', response)
    }

    if((!props.editId && response.data.result.id) || (props.editId && response.data.result[0] === 1)) {
      setMessageForm('Os dados foram salvos.')
      setTypeMessageForm('success')
      setOpenAlert(true)
      props.modalClose()
      props.editId ? props.updateTable(props.editId, props.pageId, {...form}, 'edit') : props.updateTable(response.data.result.id, props.pageId, {...form}, 'add')
    } else {
      setMessageForm('Os dados não foram salvos!')
      setTypeMessageForm('error')
      setOpenAlert(true)
    }
  };

  const [openAlert, setOpenAlert] = useState(false);
  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenAlert(false);
  };

  const handleDelete = async () => {
    const user = JSON.parse(localStorage.getItem('user'));
    const response = await axios.delete(API.blocks.delete+props.deleteId, {
      headers: {
        "x-access-token": user.token
      }
    });
    console.log('delete response: ', response)
    
    if(response.data.result[0] === 1) {
      setMessageForm('O dado foi excluído.')
      setTypeMessageForm('success')
      setOpenAlert(true)
      props.modalClose()
      props.updateTable(props.deleteId, props.pageId, null, 'delete')
    } else {
      setMessageForm('O dado não foi excluído!')
      setTypeMessageForm('error')
      setOpenAlert(true)
    }
  }

  const handleClickBlockLayout = (block) => {
    setState(prevState => ({ ...prevState, 'name': block }));
    setSelectedBlock(true)
  }

  const handleClickBackSelectLayout = () => {
    setSelectedBlock(false)
  }

  const handleClose = () => {
    props.modalClose()
    setTimeout(function(){ setSelectedBlock(false); }, 500);
  };

  const handleChangeBlock = (content) => {
    setState(prevState => ({ ...prevState, 'content': content }));
  };

  const blockComponent = () => {
    switch(name) {
      case "Col3TitleText":   return <Col3TitleText changeBlock={handleChangeBlock} content={content} />;
      default:          return ''
    }
  }

  return (
    <div>
      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
        <MuiAlert elevation={6} variant="filled" onClose={handleCloseAlert} severity={typeMessageForm} >
          {messageForm}
        </MuiAlert>          
      </Snackbar>
      <DeleteItem 
          modal={openDelete}
          modalClose={props.modalClose}
          handleDelete={handleDelete}
          handleOpenDelete={props.handleOpenDelete}
      />
      <div className="">
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={openForm}
          onClose={handleClose}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openForm}>
            <div className={classes.paper}>
              <div className="form-title-group">
                <div className="form-title">
                  Cadastro | {props.title}
                  <p className="title-select-layout">Clique em um layout para começar</p>
                </div>
                <div className="form-title-close">
                  <CloseIcon 
                    onClick={handleClose} 
                    className="button-close" 
                  />
                </div>
              </div>
              {(!selectedBlock && !props.editId) ?
              <>
              <div className="form-row">
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" onClick={() => handleClickBlockLayout('Col3TitleText')} />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
                <div className="form-group col-md-3">
                  <img src="http://fakeimg.pl/150x100/" className="img-block-layout" />
                </div>
              </div>
              </>
              :
              <>
                <div className="form-content">
                  <div className="content-form">
                    { blockComponent() }
                  </div>
                </div>
                <div className="form-button-group">
                  <div className="form-button-delete">
                    <button type="button" className="btn btn-success btn-sm" title="Voltar" onClick={() => handleClickBackSelectLayout()}>
                      <ArrowBackIcon className="button-close" />Voltar
                    </button>
                    {(!props.editId) ? '' :
                    <Button
                      variant="contained"
                      color="secondary"
                      size="small"
                      startIcon={<DeleteIcon />}
                      onClick={() => props.handleOpenDelete(props.editId)}
                      component="div"
                    >
                      Excluir
                    </Button>
                    }
                  </div>
                  <div className="form-buttons">
                    <button type="button" className="btn btn-light btn-sm" title="Cancelar" onClick={handleClose}>
                      <CancelIcon className="button-close"  />Cancelar
                    </button>
                    <button type="button" className="btn btn-primary btn-sm buttonSave" title="Salvar" onClick={handleSave}>
                      <CheckIcon className="button-close" />Salvar
                    </button>
                  </div>
                </div>
              </>}             
            </div>
          </Fade>
        </Modal>
      </div>
    </div>
  );
}

export default FormPage;