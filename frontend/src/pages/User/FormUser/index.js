import React, { useState, useEffect } from 'react'; 
import './index.scss';
import axios from 'axios';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CheckIcon from '@material-ui/icons/Check';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import API from '../../../config/api';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DeleteItem from '../../../components/DeleteItem/DeleteItem';

const useStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    width: 800,
    borderRadius: '4px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const FormTheme = (props) => {

  const classes = useStyles();

  const initialState = {
    name: '',
    username: '',
    password: '',
    email: '',
  };

  const [
    {
      name,
      username,
      password,
      email,
    },
    setState
  ] = useState(initialState);

  const clearState = () => {
    setState({ ...initialState });
  };

  const onChange = e => {
    const { name, value } = e.target;
    setState(prevState => ({ ...prevState, [name]: value }));
  };

  const [messageForm, setMessageForm] = useState('');
  const [typeMessageForm, setTypeMessageForm] = useState('');
  const [openDelete, setOpenDelete] = useState(false);
  const [openForm, setOpenForm] = useState(false);

  useEffect(function() {
    if(!props.editId && !props.deleteId && props.modal) {
      clearState()
      setOpenForm(true)
    } else if(props.deleteId && !props.modal) {
      setOpenDelete(false)
    } else if(props.deleteId && props.modal) {
      setOpenDelete(true)
    } else if(!props.deleteId && props.modal) {
      setOpenForm(true)
    } else if(!props.modal) {
      setOpenForm(false)
    }
  }, [props.modal]);

  useEffect(function() {
    async function fetchData() {
      if(props.editId) {
        const user = JSON.parse(localStorage.getItem('user'));
        let response = await axios.get(API.users.read+props.editId, {
          headers: {
            "x-access-token": user.token
          }
        });
        if(response.data.result[0].id) {
          response.data.result[0].password = "";
          setState({ ...response.data.result[0] });
        } else {
          setMessageForm('Erro ao carregar dados!')
          setTypeMessageForm('error')
          setOpenAlert(true)
          props.modalClose()
        }
      }
    }
    fetchData();
  }, [props.editId]);

  useEffect(function() {
    if(props.deleteId && props.modal) {
      setOpenForm(false)
      setOpenDelete(true)
    }
  }, [props.deleteId]);

  const handleSave = async () => {
    const form = {
      name: name,
      username: username,
      password: password,
      email: email,
    }

    console.log('save form: ', form)

    let response = [];
    const user = JSON.parse(localStorage.getItem('user'));
    if(props.editId) {
      response = await axios.put(API.users.edit+props.editId, form, {
        headers: {
          "x-access-token": user ? user.token : ''
        }
      });
      console.log('save edit response: ', response)
    } else {
      response = await axios.post(API.users.add, form, {
        headers: {
          "x-access-token": user ? user.token : ''
        }
      });
      console.log('save edit response: ', response)
    }

    if((!props.editId && response.data.result.id) || (props.editId && response.data.result[0] === 1)) {
      setMessageForm('Os dados foram salvos.')
      setTypeMessageForm('success')
      setOpenAlert(true)
      props.modalClose()
      props.editId ? props.updateTable(props.editId, form, 'edit') : props.updateTable(response.data.result.id, form, 'add')
    } else {
      setMessageForm('Os dados não foram salvos!')
      setTypeMessageForm('error')
      setOpenAlert(true)
    }
  };

  const [openAlert, setOpenAlert] = useState(false);
  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenAlert(false);
  };

  const handleDelete = async () => {
    const user = JSON.parse(localStorage.getItem('user'));
    const response = await axios.delete(API.users.delete+props.deleteId, {
      headers: {
        "x-access-token": user ? user.token : ''
      }
    });
    console.log('delete response: ', response)
    
    if(response.data.result[0] === 1) {
      setMessageForm('O dado foi excluído.')
      setTypeMessageForm('success')
      setOpenAlert(true)
      props.modalClose()
      props.updateTable(props.deleteId, null, 'delete')
    } else {
      setMessageForm('O dado não foi excluído!')
      setTypeMessageForm('error')
      setOpenAlert(true)
    }
  }

  return (
    <div>
      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
        <MuiAlert elevation={6} variant="filled" onClose={handleCloseAlert} severity={typeMessageForm} >
          {messageForm}
        </MuiAlert>          
      </Snackbar>
      <DeleteItem 
          modal={openDelete}
          modalClose={props.modalClose}
          handleDelete={handleDelete}
          handleOpenDelete={props.handleOpenDelete}
      />
      <div className="">
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={openForm}
          onClose={() => props.modalClose()}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openForm}>
            <div className={classes.paper}>
              <div className="form-title-group">
                <div className="form-title">
                  Cadastro | {props.title}
                </div>
                <div className="form-title-close">
                  <CloseIcon 
                    onClick={() => props.modalClose()} 
                    className="button-close" 
                  />
                </div>
              </div>
              <div className="form-content">
                <div className="content-form">
                  <form>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="name" className="col-form-label col-form-label-sm">Nome</label>
                        <input type="text" className="form-control form-control-sm" id="name" name="name" value={name} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="username" className="col-form-label col-form-label-sm">Usuário</label>
                        <input type="text" className="form-control form-control-sm" id="username" name="username" value={username} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="password" className="col-form-label col-form-label-sm">Senha</label>
                        <input type="text" className="form-control form-control-sm" id="password" name="password" value={password} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="email" className="col-form-label col-form-label-sm">Email</label>
                        <input type="text" className="form-control form-control-sm" id="email" name="email" value={email} onChange={onChange} />
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div className="form-button-group">
                <div className="form-button-delete">
                  {(!props.editId) ? '' :
                  <Button
                    variant="contained"
                    color="secondary"
                    size="small"
                    startIcon={<DeleteIcon />}
                    onClick={() => props.handleOpenDelete(props.editId)}
                    component="div"
                  >
                    Excluir
                  </Button>
                  }
                </div>
                <div className="form-buttons">
                  <Button
                    variant="contained"
                    color="default"
                    size="small"
                    startIcon={<CancelIcon />}
                    onClick={() => props.modalClose()}
                    component="div"
                  >
                    Cancelar
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    className="buttonSave"
                    startIcon={<CheckIcon />}
                    onClick={handleSave}
                    component="div"
                  >
                    Salvar
                  </Button>
                </div>
              </div>
            </div>
          </Fade>
        </Modal>
      </div>
    </div>
  );
}

export default FormTheme;