import React, { useState, useEffect } from 'react'; 
import './index.scss';
import axios from 'axios';
import { lighten, makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import CheckIcon from '@material-ui/icons/Check';
import CancelIcon from '@material-ui/icons/Cancel';
import CloseIcon from '@material-ui/icons/Close';
import DeleteIcon from '@material-ui/icons/Delete';
import API from '../../../config/api';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import DeleteItem from '../../../components/DeleteItem/DeleteItem';
import { useParams } from "react-router";

const useStyles = makeStyles((theme) => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1),
  },
  highlight:
    theme.palette.type === 'light'
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85),
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark,
        },
  title: {
    flex: '1 1 100%',
  },
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    width: 800,
    borderRadius: '4px',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const FormPage = (props) => {

  const { id } = useParams();
  const classes = useStyles();

  const initialState = {
    name: '',
    url: '',
    position: '',
    site_id: id,
  };

  const [
    {
      name,
      url,
      position,
      site_id,
    },
    setState
  ] = useState(initialState);

  const clearState = () => {
    setState({ ...initialState });
  };

  const onChange = e => {
    const { name, value } = e.target;
    setState(prevState => ({ ...prevState, [name]: value }));
  };

  const [messageForm, setMessageForm] = useState('');
  const [typeMessageForm, setTypeMessageForm] = useState('');
  const [openDelete, setOpenDelete] = useState(false);
  const [openForm, setOpenForm] = useState(false);

  useEffect(function() {
    if(!props.editId && !props.deleteId && props.modal) {
      clearState()
      setOpenForm(true)
    } else if(props.deleteId && !props.modal) {
      setOpenDelete(false)
    } else if(props.deleteId && props.modal) {
      setOpenDelete(true)
    } else if(!props.deleteId && props.modal) {
      setOpenForm(true)
    } else if(!props.modal) {
      setOpenForm(false)
    }
  }, [props.modal]);

  useEffect(function() {
    async function fetchData() {
      if(props.editId) {
        const user = JSON.parse(localStorage.getItem('user'));
        const response = await axios.get(API.pages.read+props.editId, {
          headers: {
            "x-access-token": user.token
          }
        });
        if(response.data.result[0].id) {
          setState({ ...response.data.result[0] });
        } else {
          setMessageForm('Erro ao carregar dados!')
          setTypeMessageForm('error')
          setOpenAlert(true)
          props.modalClose()
        }
      }
    }
    fetchData();
  }, [props.editId]);

  useEffect(function() {
    if(props.deleteId && props.modal) {
      setOpenForm(false)
      setOpenDelete(true)
    }
  }, [props.deleteId]);

  const handleSave = async () => {
    const form = {
      name: name,
      url: url, 
      position: position, 
      site_id: site_id, 
    }

    console.log('save form: ', form)
    console.log('save form props: ', props)


    let response = [];
    const user = JSON.parse(localStorage.getItem('user'));
    if(props.editId) {
      response = await axios.put(API.pages.edit+props.editId, form, {
        headers: {
          "x-access-token": user.token
        }
      });
      console.log('save edit response: ', response)
    } else {
      console.log('save form else API.pages.add: ', API.pages.add)

      response = await axios.post(API.pages.add, form, {
        headers: {
          "x-access-token": user.token
        }
      });
      console.log('save add response: ', response)
    }

    if((!props.editId && response.data.result.id) || (props.editId && response.data.result[0] === 1)) {
      setMessageForm('Os dados foram salvos.')
      setTypeMessageForm('success')
      setOpenAlert(true)
      props.modalClose()
      props.editId ? props.updateTable(props.editId, {...form}, 'edit') : props.updateTable(response.data.result.id, {...form}, 'add')
    } else {
      setMessageForm('Os dados não foram salvos!')
      setTypeMessageForm('error')
      setOpenAlert(true)
    }
  };

  const [openAlert, setOpenAlert] = useState(false);
  const handleCloseAlert = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
    setOpenAlert(false);
  };

  const handleDelete = async () => {
    const user = JSON.parse(localStorage.getItem('user'));
    const response = await axios.delete(API.pages.delete+props.deleteId, {
      headers: {
        "x-access-token": user.token
      }
    });
    console.log('delete response: ', response)
    
    if(response.data.result[0] === 1) {
      setMessageForm('O dado foi excluído.')
      setTypeMessageForm('success')
      setOpenAlert(true)
      props.modalClose()
      props.updateTable(props.deleteId, null, 'delete')
    } else {
      setMessageForm('O dado não foi excluído!')
      setTypeMessageForm('error')
      setOpenAlert(true)
    }
  }

  return (
    <div>
      <Snackbar anchorOrigin={{vertical: 'top', horizontal: 'right'}} open={openAlert} autoHideDuration={3000} onClose={handleCloseAlert}>
        <MuiAlert elevation={6} variant="filled" onClose={handleCloseAlert} severity={typeMessageForm} >
          {messageForm}
        </MuiAlert>          
      </Snackbar>
      <DeleteItem 
          modal={openDelete}
          modalClose={props.modalClose}
          handleDelete={handleDelete}
          handleOpenDelete={props.handleOpenDelete}
      />
      <div className="">
        <Modal
          aria-labelledby="transition-modal-title"
          aria-describedby="transition-modal-description"
          className={classes.modal}
          open={openForm}
          onClose={() => props.modalClose()}
          closeAfterTransition
          BackdropComponent={Backdrop}
          BackdropProps={{
            timeout: 500,
          }}
        >
          <Fade in={openForm}>
            <div className={classes.paper}>
              <div className="form-title-group">
                <div className="form-title">
                  Cadastro | {props.title}
                </div>
                <div className="form-title-close">
                  <CloseIcon 
                    onClick={() => props.modalClose()} 
                    className="button-close" 
                  />
                </div>
              </div>
              <div className="form-content">
                <div className="content-form">
                  <form>
                    <div className="form-row">
                      <div className="form-group col-md-12">
                        <label htmlFor="name" className="col-form-label col-form-label-sm">Nome da Página</label>
                        <input type="text" className="form-control form-control-sm" placeholder="" id="name" name="name" value={name} onChange={onChange} />
                      </div>
                    </div>
                    <div className="form-row">
                      <div className="form-group col-md-6">
                        <label htmlFor="url" className="col-form-label col-form-label-sm">URL</label>
                        <input type="text" className="form-control form-control-sm" placeholder="/nome-da-pagina" id="url" name="url" value={url} onChange={onChange} />
                      </div>
                      <div className="form-group col-md-6">
                        <label htmlFor="position" className="col-form-label col-form-label-sm">Posição</label>
                        <input type="number" className="form-control form-control-sm" placeholder="1" id="position" name="position" value={position} onChange={onChange} />
                      </div>
                    </div>
                  </form>
                </div>
              </div>
              <div className="form-button-group">
                <div className="form-button-delete">
                  {(!props.editId) ? '' :
                  <Button
                    variant="contained"
                    color="secondary"
                    size="small"
                    startIcon={<DeleteIcon />}
                    onClick={() => props.handleOpenDelete(props.editId)}
                    component="div"
                  >
                    Excluir
                  </Button>
                  }
                </div>
                <div className="form-buttons">
                  <Button
                    variant="contained"
                    color="default"
                    size="small"
                    startIcon={<CancelIcon />}
                    onClick={() => props.modalClose()}
                    component="div"
                  >
                    Cancelar
                  </Button>
                  <Button
                    variant="contained"
                    color="primary"
                    size="small"
                    className="buttonSave"
                    startIcon={<CheckIcon />}
                    onClick={handleSave}
                    component="div"
                  >
                    Salvar
                  </Button>
                </div>
              </div>
            </div>
          </Fade>
        </Modal>
      </div>
    </div>
  );
}

export default FormPage;