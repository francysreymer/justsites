const UserModel = async (Sequelize) => {
  const database = require('../../config/database.json');
  const sequelize = await new Sequelize(database.url);

  const User = sequelize.define('user', 
    {
      id: {
        type: Sequelize.INTEGER,
        required: true,
        primaryKey: true,
        autoIncrement: true,
      },
      name: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      username: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      password: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      email: {
        type: Sequelize.STRING,
        required: true,
        allowNull: false
      },
      active: {
        type: Sequelize.BOOLEAN,
        allowNull: false,
        defaultValue: true
      }, 
    },
    { 
      tableName: 'users',
      freezeTableName: false,
      timestamps: true,
      createdAt: 'created_at',
      updatedAt: 'updated_at',  
    }
)

await User.sync()

return {User, sequelize}
}

module.exports = UserModel