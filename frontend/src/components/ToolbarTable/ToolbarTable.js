import React from 'react'; 
import './ToolbarTable.scss';
import AddIcon from '@material-ui/icons/Add';

const ToolbarTable = (props) => {

  return (
    <div className="toolbar-table">
      <div className="title">
        <h6>{props.title}</h6>
      </div>
      <div className="button">
        <button type="button" className="btn btn-primary btn-sm btn-add" title={props.textButton} onClick={props.handleOpen}>
          <AddIcon color="inherit" />{props.textButton}
        </button>
      </div>
    </div>
  );
}

export default ToolbarTable;