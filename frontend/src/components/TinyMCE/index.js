import React from 'react';
import { Editor } from '@tinymce/tinymce-react';

const EditorText = (props) => {
    const handleEditorChange = (content, editor) => {
        props.changeEditorText(content)
    }

    let init;
    switch(props.type) {
        case "title":   
            init = {
                height: 150,
                menubar: false,
                branding: false,
                plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
                ],
                toolbar:
                'bold italic forecolor backcolor | '
            }
            break;
        case "text":   
            init = {
                height: 400,
                menubar: false,
                branding: false,
                plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
                ],
                toolbar:
                'bold italic forecolor backcolor | '
            }
            break;
        default:          
            init = {
                height: 500,
                menubar: false,
                plugins: [
                'advlist autolink lists link image charmap print preview anchor',
                'searchreplace visualblocks code fullscreen',
                'insertdatetime media table paste code help wordcount'
                ],
                toolbar:
                'undo redo | formatselect | bold italic backcolor | \
                alignleft aligncenter alignright alignjustify | \
                bullist numlist outdent indent | removeformat | help'
            }
    }

    return (
        <Editor
            apiKey='s8fvs1vs37jltif7tn96kok3udg1n662u83e1pgx6m9t2wbg'
            initialValue={props.initialValue}
            init={init}
            onEditorChange={handleEditorChange}
        />
    )
}

export default EditorText;
