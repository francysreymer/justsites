import React, { useEffect, useState } from 'react'
import { withStyles } from '@material-ui/core/styles';
import axios from 'axios';
import API from '../../config/api';
import './index.scss';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import DescriptionIcon from '@material-ui/icons/Description';
import FormPage from './FormPage';
import FormBlock from './FormBlock';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import AddIcon from '@material-ui/icons/Add';
import Parser from 'html-react-parser';

const Accordion = withStyles({
  root: {
    width: '256px',
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      marginBottom: 'auto',
      marginTop: 'auto',
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: '#fff',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    minHeight: 36,
    '&$expanded': {
      minHeight: 36,
    },
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    textAlign: 'center',
  },
}))(MuiAccordionDetails);

export default function Page(props) {

  const [expanded, setExpanded] = useState('panel1');

  const handleChange = (panel) => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false);
  };

  const [dataRows, setDataRows] = React.useState([]);
  const [blocks, setBlocks] = React.useState([]);

  useEffect(() => {
    async function fetchData() {
      const user = JSON.parse(localStorage.getItem('user'));
      const response = await axios.get(API.pages.index, {
        headers: {
          "x-access-token": user.token
        }
      });
      console.log('pages response.data.data: ', response.data.data)
      setDataRows(response.data.data);
    }
    fetchData();
  }, [])

  const [open, setOpen] = useState(false);
  const handleOpen = () => {
    setEditId('');
    setDeleteId('');
    setOpen(true);
  };

  const [editId, setEditId] = useState('');
  const handleEdit = (id) => {
    console.log('handleEdit id: ', id)
    setEditId(id);
    setDeleteId('');
    setOpen(true);
  };

  const [deleteId, setDeleteId] = useState('');
  const handleDelete = (id) => {
    setEditId('');
    setDeleteId(id);
    setOpen(true);
  };

  const handleCloseModal = () => {
    setOpen(false)
  }

  const updateTable = (id, row, option) => {
    let aux = Object.assign([], dataRows);
    switch(option) {
      case 'add':
        aux.push({...row, id: id})
        break;
      case 'edit':
        aux = aux.filter(row => row.id !== id);
        aux.push({...row, id: id})
        break;
      case 'delete':
        aux = aux.filter(row => row.id !== id);
        break;
    }
    setDataRows(aux);
  }

  const [openBlock, setOpenBlock] = useState(false);
  const [editIdBlock, setEditIdBlock] = useState('');
  const [deleteIdBlock, setDeleteIdBlock] = useState('');
  const [pageId, setPageId] = useState('');

  const handleOpenBlock = (pageId) => {
    setEditIdBlock('');
    setDeleteIdBlock('');
    setPageId(pageId);
    setOpenBlock(true);
  };

  const handleEditBlock = (id, pageId) => {
    console.log('handleEditBlock id: ', id)
    setEditIdBlock(id);
    setDeleteIdBlock('');
    setPageId(pageId);
    setOpenBlock(true);
  };

  const handleDeleteBlock = (id) => {
    setEditIdBlock('');
    setDeleteIdBlock(id);
    setOpenBlock(true);
  };

  const handleCloseModalBlock = () => {
    setOpenBlock(false)
  }

  const updateTableBlock = (id, pageId, row, option) => {

    const objIndex = dataRows.findIndex((obj => obj.id === pageId));
    let aux = Object.assign([], dataRows);

    switch(option) {
      case 'add':
        if(!aux[objIndex].blocks) {
          aux[objIndex].blocks = []
        }
        aux[objIndex].blocks.push({...row, id: id})
        break;
      case 'edit':
        aux[objIndex].blocks = aux[objIndex].blocks.filter(row => row.id !== id);
        aux[objIndex].blocks.push({...row, id: id})
        break;
      case 'delete':
        aux[objIndex].blocks = aux[objIndex].blocks.filter(row => row.id !== id);
        break;
    }
    setDataRows(aux);
  }

  const handleViewBlocks = (id) => {
    const objIndex = dataRows.findIndex((obj => obj.id === id));
    setBlocks(dataRows[objIndex].blocks)

    setExpanded(id);
  }

  return (
    <>
      <FormPage 
        modal={open}
        modalClose={handleCloseModal}
        editId={editId}
        deleteId={deleteId}
        handleOpenDelete={handleDelete}
        updateTable={updateTable}
        title="Página"
      />
      <FormBlock 
        modal={openBlock}
        modalClose={handleCloseModalBlock}
        editId={editIdBlock}
        deleteId={deleteIdBlock}
        handleOpenDelete={handleDeleteBlock}
        updateTable={updateTableBlock}
        title="Bloco"
        pageId={pageId}
      />
      <nav className="sideBar">
        <a className="back" href="javascript:history.back()"><div className="pages-back"><ChevronLeftIcon />ADMIN</div></a>
        <div className="pages-title">PÁGINAS DO SITE</div>
          {dataRows.map((row, index) => {
              return (
                <>
                  <a className="menu-pages" href="#">
                    <div className="list-pages" key={row.id}>
                      <div className="icon-pages" onClick={() => handleViewBlocks(row.id)}><DescriptionIcon className="icon" color="inherit" fontSize="small" /></div>
                      <div className="name-pages" onClick={() => handleViewBlocks(row.id)}>{row.name}</div>
                      <div className="button-pages">
                        <button type="button" className="btn btn-primary btn-sm btn-edit-page btn-actions-page" title="Editar Página" onClick={() => handleEdit(row.id)}>
                          <EditIcon color="inherit" fontSize="small" />
                        </button>
                        <button type="button" className="btn btn-danger btn-sm btn-delete-page btn-actions-page" title="Excluir Página" onClick={() => handleDelete(row.id)}>
                          <DeleteIcon color="inherit" fontSize="small" />
                        </button>            
                      </div>
                    </div>
                  </a>
                </>
              );
          })}
        <div className="div-page-buton">
          <button type="button" className="btn btn-primary btn-sm btn-add-page" title="Adicionar Página" onClick={handleOpen}>
            <AddIcon color="inherit" fontSize="small" /> ADICIONAR PÁGINA
          </button>
        </div>      
      </nav>
      <div className="content">
        <div className="content-blocks">
          <div className="div-accordion">
            {dataRows.map((row, index) => {
              return (
                <>
                  <Accordion square expanded={expanded === row.id} onChange={handleChange(row.id)} key={row.id}>
                    <AccordionSummary aria-controls="panel1d-content" id={row.id} key={row.id} expandIcon={<ExpandMoreIcon />}>
                      <Typography>{row.name}</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                      <Typography>
                        {row.blocks && row.blocks.length > 0 && row.blocks.map((block) => {
                          return (
                            <img key={block.id} src="http://fakeimg.pl/150x100/" className="img-block-layout" onClick={() => handleEditBlock(block.id, row.id)} />
                          );
                        })}
                        <button type="button" className="btn btn-outline-success btn-sm btn-add-block" title="Adicionar Bloco" onClick={() => handleOpenBlock(row.id)}>
                          <AddIcon color="inherit" fontSize="small" /> Adicionar Bloco
                        </button>  
                      </Typography>
                    </AccordionDetails>
                  </Accordion>
                </>
              );
            })}
          </div>
          <div className="blocks">
            <div className="block">
            {blocks.map((block) => {
              return (
                <>
                {Parser(block.content.html)}
                </>
              );
            })}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
