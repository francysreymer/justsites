/* ADMIN */
CREATE TABLE companies (
	id INT UNSIGNED AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	document VARCHAR(255),
	responsible VARCHAR(255),
	responsible_email VARCHAR(255),
	responsible_phone VARCHAR(255),

	zip_code VARCHAR(255),
	address VARCHAR(255),
	number VARCHAR(255),
	complement VARCHAR(255),
	neighborhood VARCHAR(255),
	city VARCHAR(255),
	state VARCHAR(255),
	
	observation TEXT,
	created_at DATETIME NOT NULL,
	updated_at DATETIME DEFAULT NULL,
	active BOOLEAN DEFAULT TRUE NOT NULL,
	PRIMARY KEY (id)
); 

CREATE TABLE themes (
	id INT UNSIGNED AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	css_file text,
	created_at DATETIME NOT NULL,
	updated_at DATETIME DEFAULT NULL,
	active BOOLEAN DEFAULT TRUE NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE sites (
	id INT UNSIGNED AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	domain VARCHAR(255),
	created_at DATETIME NOT NULL,
	updated_at DATETIME DEFAULT NULL,
	active BOOLEAN DEFAULT TRUE NOT NULL,
	theme_id INT UNSIGNED NOT NULL,
	company_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (theme_id) REFERENCES themes (id),
	FOREIGN KEY (company_id) REFERENCES companies (id)
);

CREATE TABLE pages (
	id INT UNSIGNED AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	url VARCHAR(255) NOT NULL,
	created_at DATETIME NOT NULL,
	updated_at DATETIME DEFAULT NULL,
	active BOOLEAN DEFAULT TRUE NOT NULL,
	position INT UNSIGNED NOT NULL,
	site_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (site_id) REFERENCES sites (id)
);

CREATE TABLE blocks (
	id INT UNSIGNED AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	content JSON NOT NULL,
	created_at DATETIME NOT NULL,
	updated_at DATETIME DEFAULT NULL,
	active BOOLEAN DEFAULT TRUE NOT NULL,
	page_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (page_id) REFERENCES pages (id)
);

/* COMPANY */
CREATE TABLE categories (
	id INT UNSIGNED AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	created_at DATETIME NOT NULL,
	updated_at DATETIME DEFAULT NULL,
	active BOOLEAN DEFAULT TRUE NOT NULL,
	company_id INT UNSIGNED NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (company_id) REFERENCES companies (id)
);

CREATE TABLE blogs (
	id INT UNSIGNED AUTO_INCREMENT NOT NULL,
	title VARCHAR(255) NOT NULL,
	content text NOT NULL,
	created_at DATETIME NOT NULL,
	updated_at DATETIME DEFAULT NULL,
	active BOOLEAN DEFAULT TRUE NOT NULL,
	company_id INT UNSIGNED NOT NULL,
	categories JSON,
	PRIMARY KEY (id),
	FOREIGN KEY (company_id) REFERENCES companies (id)
);

CREATE TABLE users (
	id INT UNSIGNED AUTO_INCREMENT NOT NULL,
	name VARCHAR(255) NOT NULL,
	username VARCHAR(255) NOT NULL,
	password VARCHAR(255) NOT NULL,
	email VARCHAR(255) NOT NULL,
	created_at DATETIME NOT NULL,
	updated_at DATETIME DEFAULT NULL,
	active BOOLEAN DEFAULT TRUE NOT NULL,
	PRIMARY KEY (id)
);

##################################################################
Sprint 1 - APIs - BackEnd
a) Criar banco - 2 horas - DONE
b) API companies - 1 dia - DONE
c) API themes - 1 dia - DONE
d) API sites - 1 dia - DONE
e) API pages - 1 dia - DONE
f) API categories - 1 dia - DONE
g) API blogs - 1 dia - DONE

Sprint 2 - FrontEnd - SuperAdmin
a) Cadastro de companies - 1 dia - DONE
b) Cadastro de themes - 1 dia - DONE
c) Cadastro de sites - 1 dia - DONE
d) Cadastro de pages/blocks - 4 dias
e) Tela de login - 1 dia

Sprint 3 - FrontEnd - Admin
a) Cadastro de categories - 1 dia - DONE
b) Cadastro de blogs - 1 dia - DONE
c) Tela de login - 2 horas

Sprint 4 - Autenticação
a) Autenticação SuperAdmin - 1 dia
b) Autenticação Admin (Company) - 1 dia

Sprint 5 - FrontEnd - Site
a) Montar páginas/estrutura do site - 2 dias
b) CSS do site - Tema 1 - 2 dias
c) CSS do site - Tema 2 - 1 dia

Sprint 6 - Servidor
a) Configurar servidor Amazon - 5 dias
b) Subir aplicação - 1 dia
c) Configurar virtual hosts - 3 dias